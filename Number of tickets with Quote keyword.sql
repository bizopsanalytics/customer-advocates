
SELECT
year(ji.created) as year,
month(ji.created) as month,
case 
        when 
        (
                ji.description like '%quote%' 
                or ji.summary like '%quote%'
                or ja.actionbody like '%quote%'
        ) 
        then 'Quotes'
else    'Something Else'
end as  InvoiceCategory,
count(distinct ji.issuenum)
FROM raw_sac_vnext.jiraissue ji
LEFT JOIN raw_sac_vnext.project p ON ji.project = p.id
LEFT JOIN raw_sac_vnext.resolution r on ji.resolution = r.id
LEFT JOIN raw_sac_vnext.jiraaction ja on (ja.issueid = ji.issuenum)
WHERE p.pkey = 'CA'
AND (r.pname not in ('Duplicate','Spam') or r.pname is null)
and year(ji.created) in (2016,2017)
group by 1,2,3
order by 1,2,3
